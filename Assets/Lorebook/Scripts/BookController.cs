﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BookController : MonoBehaviour
{
    public PagesController pagesController;
	public Animator bookAnim;
	public List<Animator> pageAnims;
	public CoverEvents coverEvents;
	public PageEvents pagePrev;
	public PageEvents pageNext;

    public GameObject leftButton;   // leftButton.GetComponent<LeftClickButton>();
    public GameObject rightButton;  // rightButton.GetComponent<RightClickButton>();

    public bool IsPreOpened = false;
	public bool IsOpened = false;
    static bool oneTime = true;
    public GameObject leftButtonObj;
    public GameObject rightButtonObj;
    public GameObject coverCollider;

    /// <summary>
    /// Function to initiate gameobject at the starting
    /// </summary>
    public void Start()
    {
       /*leftButtonObj = GameObject.Find("LeftSphereCommands");
        rightButtonObj = GameObject.Find("RightSphereCommands"); */
        leftButtonObj  = GameObject.Find("Left");
        rightButtonObj = GameObject.Find("Right");
        //leftButtonObj.SetActive(false);
    }  

    /// <summary>
    /// Function to start opennin animation
    /// </summary>
    public void startOpening()
    {   
		IsPreOpened = true;
		bookAnim.SetBool("IsPreOpened", IsPreOpened);
		setPageAnimsBool("IsPreOpened", IsPreOpened);
    }

    /// <summary>
    /// Stop the opening of the book.
    /// </summary>
	public void stopOpening()
    {
		IsPreOpened = false;
		bookAnim.SetBool("IsPreOpened", IsPreOpened);
		setPageAnimsBool("IsPreOpened", IsPreOpened);
	}

    /// <summary>
    /// Fucntion to open book
    /// </summary>
	public void openBook()
    {

       // leftButtonObj.SetActive(true);
        IsOpened = true;  
		IsPreOpened = true;
		bookAnim.SetBool("IsPreOpened", IsPreOpened);
		bookAnim.SetBool("IsOpened", IsOpened);
		setPageAnimsBool("IsPreOpened", IsPreOpened);
		setPageAnimsBool("IsOpened", IsOpened);
        pageNext.setColliderEnabled(true);
        pagePrev.setColliderEnabled(true);
        coverCollider.GetComponent<BoxCollider>().enabled = false;

        if (oneTime == true)
        {
            StartCoroutine(pagesController.CoroutineAction());
            oneTime = false;
        }

    }

    /// <summary>
    /// Toggling animation flag
    /// </summary>
    /// <param name="flag">name of the flag</param>
    /// <param name="val">boolen value</param>
	public void setPageAnimsBool(string flag, bool val)
    {
		foreach (Animator anim in pageAnims)
        {
			anim.SetBool(flag, val);
		}
	}

    /// <summary>
    /// Turn to next page.
    /// </summary>
	public void nextPage()
    {
        if (!IsOpened)
        {   
            openBook();
            return;
        }
		pageAnims[pagesController.returnedAnimatedPage].Play("pageNext");
		pagesController.pageFlip(true);
	}

    /// <summary>
    /// Turn to previos page
    /// </summary>
	public void prevPage()
    {
       if (!IsOpened) {
            return;
       }

       if (pagesController.currentTMPPage <= 1) {
			IsOpened = false;
            //leftButtonObj.SetActive(false);
            bookAnim.SetBool("IsOpened", IsOpened);
			setPageAnimsBool("IsOpened", IsOpened);
			pageNext.setColliderEnabled(false);
            coverCollider.GetComponent<BoxCollider>().enabled = true;
            // pagesController.flippedPages.transform.localPosition = new Vector3(0.0f, -4.991f, 0.0f);
        } else {
			pagesController.pageFlip(false);
			pageAnims[Mathf.Abs(pagesController.returnedAnimatedPage % 3)].Play("pagePrev");

		}
	}

    /// <summary>
    /// Exit the application
    /// </summary>
    public void closeApplication()
    {
        Debug.Log("CloseApplication function called");
        Debug.Log(Application.platform);
        if (Application.platform == RuntimePlatform.WindowsEditor) // if is user on windows editor
        {
            // EditorApplication.Exit(0);           //will close the editor
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false; //Will pause the scene
#endif
        } else {
            Application.Quit();         // When runing the app on device
        }
    }

}
