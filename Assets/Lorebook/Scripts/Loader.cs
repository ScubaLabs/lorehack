﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
	public string filename = "examplebook.html";
    public GameObject filenamestring;
    private delegate void Callback (string data);
	public string loadedText;

    void Start ()
    {
        // Debug.Log("Printing dont destory on load");
        // Application.LoadLevelAdditive("YourNextScene");
        // Debug.Log(filenamestring);
        // Debug.Log(GameObject.Find("DontDestroyOnLoad"));
        // Application.LoadLevel("DontDestroyOnLoad");

        /*Transform[] hinges = GameObject.FindObjectsOfType(typeof(Transform)) as Transform[];
            Debug.Log(hinges);
            Debug.Log(GameObject.Find("JungleBook"));*/
        //gameObject[] goArray = SceneManager.GetSceneByName("DontDestroyOnLoad").GetRootGameObjects();
        //SceneManager.SetActiveScene(Scene DontDestroyOnLoad);

        /* SceneManager.LoadScene("DontDestroyOnLoad", LoadSceneMode.Additive);
         SceneManager.SetActiveScene(SceneManager.GetSceneByName("DontDestroyOnLoad"));
         GameObject[] goArray = SceneManager.GetSceneByName("DontDestroyOnLoad").GetRootGameObjects();
         Debug.Log(goArray);
          foreach (GameObject arrayObj in goArray){
              Debug.Log(arrayObj);
         } */

        Debug.Log(GameObject.Find("BookManager").tag);
        LoadFile(GameObject.Find("BookManager").tag);	 
    }

	public void LoadFile(string bookname)
    {
        filename = bookname+".html";
        string filePath = Application.streamingAssetsPath + "/" + filename;
		
        #if UNITY_EDITOR
		filePath = "file://" +  filePath;
        Debug.Log(filePath);
        #endif
		StartCoroutine(getFile(filePath, OnTextLoaded));
	}

	IEnumerator getFile(string assetPath, Callback callback)
    {
		WWW data = new WWW(assetPath);
		yield return data;
		if(string.IsNullOrEmpty(data.error)) {
			callback(data.text);
		}
	}

	public void OnTextLoaded(string text)
    {
		loadedText = text;
		gameObject.GetComponent<PagesController>().init(loadedText);
	}

}
