﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public static class WaitFor
{
    public static IEnumerator Frames(int frameCount)
    {
        while (frameCount > 0)
        {
            frameCount--;
            yield return null;
        }
    }
}

public class PagesController : MonoBehaviour {

	public TextMeshPro pageTextMeshPro;
	public CameraTextureBaker pageCamera;
	public BookController bookController;
	public string TMProText;

	public int currentTMPPage = 1;
	public int currentAnimatedPage = 0;
	public int returnedAnimatedPage = 0;

	public List<SkinnedMeshRenderer> animatedPages;
    public SkinnedMeshRenderer actualLeftPage;
    public SkinnedMeshRenderer actualRightPage;
    public GameObject flippedPages;
    public float pageOffsetY = -0.0025f;

    public GameObject page_only;
    public GameObject page_only_1;
    public GameObject page_only_2;

    float offsetOnPageFlip = -0.015f;

	void Start(){
		
	}

    

    public IEnumerator CoroutineAction()
    {
        // do some actions here  
        //yield return StartCoroutine(WaitFor.Frames(5)); // wait for 5 frames
        // do some actions after 5 frames
        yield return new WaitForSeconds(1);
        //-4.991
       // flippedPages.transform.Translate(0.0f, -0.035f, 0.0f);
        //flippedPages.SetActive(false);
    }

    public void init(string text){
		TMProText = TextFormatter.formatText(text);
		setTextInMeshProText(TMProText);
		pageInit(true);
	}

	public void setTextInMeshProText(string text) {
		pageTextMeshPro.SetText(text);
		pageTextMeshPro.text = text;
	}

	public void pageInit(bool next){
		Material[] materials;
		materials = animatedPages[returnedAnimatedPage].sharedMaterials;
		int page = currentTMPPage;
		if (next) {
			for (int i = (materials.Length); i > 0; i--) {
				materials[i - 1].SetTexture("_MainTex", getTextMeshProPage(page));
				page++;
			}
		} else {
			for (int i = 0; i < materials.Length; i++) {
				materials[i].SetTexture("_MainTex", getTextMeshProPage(page));
				page--;
			}
		}
        


        //actualLeftPage.material.SetTexture("_MainTex", getTextMeshProPage(1));
        //actualRightPage.material.SetTexture("_MainTex", getTextMeshProPage(2));
        //actualRightPage.materials[1].SetTexture("_MainTex", getTextMeshProPage(1));
    }

	public void pageFlip(bool next){
      // flippedPages.SetActive(true);

        int page = currentTMPPage;
		if (next) {
			Material[] materials = animatedPages[Mathf.Abs((currentAnimatedPage + 1) % 3)].sharedMaterials;
			for (int i = (materials.Length); i > 0; i--) {
				materials[i - 1].SetTexture("_MainTex", getTextMeshProPage(page + 2));
				currentTMPPage++;
				page++;
			}
		} else {
			Material[] materials = animatedPages[Mathf.Abs((currentAnimatedPage - 2) % 3)].sharedMaterials;
			for (int i = 0; i < materials.Length; i++) {
				if ((page - 3) > 0 ) {
					materials[i].SetTexture("_MainTex", getTextMeshProPage(page - 3));
				}
				currentTMPPage--; 
				page--;
			}
		}
		animatedPageReplacer(next, true);
        //if(currentTMPPage > 1)
            //flippedPages.transform.Translate(0.0f, offsetOnPageFlip, offsetOnPageFlip*4f - 0.04f);
        //StartCoroutine(CoroutineAction());
        //flippedPages.SetActive(false);
    }

	public void meshEnabler(){
		foreach (SkinnedMeshRenderer smr in animatedPages){
			smr.GetComponent<Transform>().parent.transform.position = Vector3.zero;
		}

        page_only.GetComponent<Transform>().localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        page_only_1.GetComponent<Transform>().localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        page_only_2.GetComponent<Transform>().localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    }

    public void animatedPageReplacer(bool next, bool is_increase){
        Debug.Log(currentAnimatedPage);
        meshEnabler();
		if (next) {
			bookController.pageAnims[Mathf.Abs((currentAnimatedPage + 1) % 3) ].CrossFade("pageOpened", 0f);
            int val = Mathf.Abs((currentAnimatedPage + 2) % 3);
            if (val == 0)
            {
                page_only.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY, 0.0f);
            }
            else if (val == 1)
            {
                page_only_1.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY, 0.0f);
            }
            else if (val == 2)
            {
                page_only_2.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY, 0.0f);
            }

            //animatedPages[Mathf.Abs((currentAnimatedPage + 2) % 3) ].GetComponent<Transform>().parent.transform.position = new Vector3 (0, pageOffsetY, 0);
            if (is_increase) 
			currentAnimatedPage++;
		} else {
            //animatedPages[Mathf.Abs((currentAnimatedPage) % 3) ].GetComponent<Transform>().parent.transform.position = new Vector3 (0, pageOffsetY, 0);

            int val = Mathf.Abs((currentAnimatedPage) % 3);
            if (val == 0)
            {
                page_only.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY, 0.0f);
            }
            else if (val == 1)
            {
                page_only_1.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY, 0.0f);
            }
            else if (val == 2)
            {
                page_only_2.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY, 0.0f);
            }

            if (currentAnimatedPage == 1) {
				bookController.pageAnims[Mathf.Abs((currentAnimatedPage + 1) % 3) ].CrossFade("pageOpened", 0f);
                //animatedPages[Mathf.Abs((currentAnimatedPage + 1) % 3) ].GetComponent<Transform>().parent.transform.position = new Vector3 (0, pageOffsetY*2 , 0);

                val = Mathf.Abs((currentAnimatedPage+1) % 3);
                if (val == 0)
                {
                    page_only.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY*2, 0.0f);
                }
                else if (val == 1)
                {
                    page_only_1.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY * 2, 0.0f);
                }
                else if (val == 2)
                {
                    page_only_2.GetComponent<Transform>().localPosition = new Vector3(0.0f, pageOffsetY * 2, 0.0f);
                }

            } else {
				bookController.pageAnims[Mathf.Abs((currentAnimatedPage + 1) % 3) ].CrossFade("pageOpenedMirror", 0f);
			}
			if (is_increase) 
			currentAnimatedPage--;
		}
		returnedAnimatedPage = Mathf.Abs(currentAnimatedPage % 3);

    }

	public RenderTexture getTextMeshProPage(int page){
		pageTextMeshPro.GetComponent<MeshRenderer>().enabled = true;
		pageTextMeshPro.pageToDisplay = page;
		RenderTexture tex = pageCamera.getCameraPage();
		pageTextMeshPro.GetComponent<MeshRenderer>().enabled = false;
		return tex;
	}
}