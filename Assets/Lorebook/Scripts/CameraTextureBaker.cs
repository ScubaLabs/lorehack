﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraTextureBaker : MonoBehaviour
{

	public int width = 512;
	public int height = 512;

	public RenderTexture getCameraPage()
    {
		Camera activeCamera = GetComponent<Camera>();
		RenderTexture rt = new RenderTexture(width, height, 24);
		activeCamera.targetTexture = rt;
		activeCamera.Render();
		RenderTexture.active = rt;
		activeCamera.targetTexture = null;
		RenderTexture.active = null;
		return rt;
	}
}
