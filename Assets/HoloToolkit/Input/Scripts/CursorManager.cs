﻿using HoloToolkit;
using UnityEngine;

/// <summary>
/// CursorManager class takes Cursor GameObjects.
/// One that is on Holograms and another off Holograms.
/// Shows the appropriate Cursor when a Hologram is hit.
/// Places the appropriate Cursor at the hit position.
/// Matches the Cursor normal to the hit surface.
/// </summary>
public class CursorManager : Singleton<CursorManager>
{
    [Tooltip("Drag the Cursor object to show when it hits a hologram.")]
    public GameObject CursorOnHolograms;

    [Tooltip("Drag the Cursor object to show when it does not hit a hologram.")]
    public GameObject CursorOffHolograms;

    [Tooltip("Cursor object to show left hand icon on left page hologram")]
    public GameObject CursorLeftHolograms;

    [Tooltip("Cursor object to show rught hand icon on right page hologram")]
    public GameObject CursorRightHolograms;

    void Awake()
    {
        if (CursorOnHolograms == null || CursorOffHolograms == null)
        {
            return;
        }

        CursorOnHolograms.SetActive(false);
        CursorOffHolograms.SetActive(false);
    }

    void Update()
    {
    
        if (GazeManager.Instance == null || CursorOnHolograms == null || CursorOffHolograms == null)
        {
            return;
        }

        if (GazeManager.Instance.Hit)
        {
           // Debug.Log(GazeManager.Instance.HitInfo.collider.gameObject.name);
            if (GazeManager.Instance.HitInfo.collider.gameObject.name == "pageNext" || GazeManager.Instance.HitInfo.collider.gameObject.name == "pagePrev"){
                CursorOnHolograms.SetActive(false);
                CursorOffHolograms.SetActive(false);
            } else {
                CursorOnHolograms.SetActive(true);
                CursorOffHolograms.SetActive(false);
            }
        } else {
            CursorOnHolograms.SetActive(false);
            CursorOffHolograms.SetActive(true);
        }

        gameObject.transform.position = GazeManager.Instance.Position;
        gameObject.transform.up = GazeManager.Instance.Normal;
    }
}