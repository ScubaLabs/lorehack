﻿using UnityEngine;
using System.Collections;

public class HandleAudio : MonoBehaviour
{
    public AudioClip SoundToPlay;
    public float Volume;
    AudioSource audio;
    public bool alreadyPlayed = false;
   
    void Start ()
    {
        audio = GetComponent<AudioSource>();
    }

    void OnSelect()
    {
        alreadyPlayed = !alreadyPlayed;
        if (alreadyPlayed){
            audio.Play();
        }
        else{
           audio.Pause();
        }
    }
}

