﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class TapToPlaceParent : MonoBehaviour
{
    bool placing = false;
    public GameObject bookCover;
    public GameObject cursorGameObj;
    public GameObject dragGameObj;

    GestureRecognizer recognizer;

    /// <summary>
    /// Called the for initiating first time
    /// </summary>
    void Start()
    {
        recognizer = new GestureRecognizer();
        recognizer.TappedEvent += (source, tapCount, ray) =>
        {
            if(placing)
            {
                placing = false;
                recognizer.StopCapturingGestures();
                Debug.Log("drag not placing inside recognizer");
                SpatialMapping.Instance.DrawVisualMeshes = false;
            }
            
        };
     }

    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        Debug.Log("drag function called");
        // On each Select gesture, toggle whether the user is in placing mode.
        placing = true;

        // If the user is in placing mode, display the spatial mapping mesh.
        if (placing)
        {
            Debug.Log("drag  placing");
            SpatialMapping.Instance.DrawVisualMeshes = true;
            recognizer.StartCapturingGestures();
        }
        // If the user is not in placing mode, hide the spatial mapping mesh.
        else
        {
            Debug.Log("drag not placing");
            SpatialMapping.Instance.DrawVisualMeshes = false;
            recognizer.StopCapturingGestures();
        }
    }

    // Do a raycast into the world that will only hit the Spatial Mapping mesh.
    // var headPosition = Camera.main.transform.position;
    // var gazeDirection = Camera.main.transform.forward;

    // RaycastHit hitInfo;
    // if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
    //    30.0f, SpatialMapping.PhysicsRaycastMask))
    //{
    //    // Move this object's parent object to
    //    // where the raycast hit the Spatial Mapping mesh.
    //    this.transform.parent.position = hitInfo.point;

    //    // Rotate this object's parent object to face the user.
    //    Quaternion toQuat = Camera.main.transform.localRotation;
    //    toQuat.x = 0;
    //    toQuat.z = 0;
    //    this.transform.parent.rotation = toQuat;
    //}


    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        // If the user is in placing mode,
        // update the placement to match the user's gaze.

        if (placing)
        {
            // Do a raycast into the world that will only hit the Spatial Mapping mesh.
            var headPosition = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;
            RaycastHit hitInfo;

            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
                30.0f, SpatialMapping.PhysicsRaycastMask))
            {

                // Move this object's parent object to
                // where the raycast hit the Spatial Mapping mesh.
                //  this.transform.parent.position = hitInfo.point;
                var pt = hitInfo.point;
                pt.z = bookCover.transform.position.z;
                //bookCover.transform.position = pt;
                bookCover.transform.position = hitInfo.point;
                // cursorGameObj.transform.localPosition = dragGameObj.transform.localPosition;
                // Rotate this object's parent object to face the user.
                Quaternion toQuat = Camera.main.transform.localRotation;
                toQuat.x = 0;
                toQuat.z = 0;
                // this.transform.parent.rotation = toQuat;
                bookCover.transform.rotation = toQuat;
                bookCover.transform.Rotate(0, 270, 75);
                //bookCover.transform.position.Set(bookCover.transform.position.x, bookCover.transform.position.y, 1.5f);
            }
        }
    }
}