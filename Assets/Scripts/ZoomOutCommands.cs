﻿using UnityEngine;
using System.Collections;

public class ZoomOutCommands : MonoBehaviour
{
    public GameObject coverObject;
    public GameObject actionBarObject;
    private float targetZoomOutMax = 0.3F;
    private float targetZoomOutMin = 0.05F;

    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        Debug.Log("Zoom out button OnSelect called");
        Debug.Log(coverObject.transform.localScale);
        Debug.Log(coverObject.transform.localPosition);
        Debug.Log(coverObject.transform.localScale.x);
        Debug.Log(coverObject.transform.localScale.x+">> "+targetZoomOutMin);

        if (coverObject.transform.localScale.x > targetZoomOutMin)
        {
            coverObject.transform.localScale -= new Vector3(0.1F/4f, 0.1F / 4f, 0.1F / 4f);

        } else {
            //coverObject.transform.localScale    = new Vector3(0.1F, 0.1F, 0.1F);
            //coverObject.transform.localPosition = new Vector3(0.1F, 0.1F, 2.5F);
            //actionBarObject.transform.localScale = new Vector3(1, 1, 1);
            //actionBarObject.transform.localPosition = new Vector3(0.4829629F, 0.1294096F, 0);
        }

    }
}
