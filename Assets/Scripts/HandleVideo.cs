﻿using UnityEngine;
using System.Collections;

public class HandleVideo : MonoBehaviour
{
   public GameObject bookController;
   public GameObject videoCanvas;
   public PlayVideo display;
   public bool playing = false;

   /// <summary>
   ///  Called first time for initiating
   /// </summary>   
   void Start()
   {
        videoCanvas.SetActive(false);
   }

    /// <summary>
    /// Called when user performed a tap event on video button
    /// </summary> 
    void OnSelect()
   {
        Debug.Log("video canvas OnSelect called");
        playing = !playing;
        if (playing)
        {
            Debug.Log("playing true");
            display.StartMovieTexture();
            videoCanvas.SetActive(true);
        }
        else
        {
            Debug.Log("playing false");
            display.StopMovieTexture();
            videoCanvas.SetActive(false);
        }


    }
}
