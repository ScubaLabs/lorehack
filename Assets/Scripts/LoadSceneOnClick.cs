﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour
{

    public void LoadByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }
    

    public void LoadByBookName(string bookname)
    {
        //GameObject manager = new GameObject("BookManager");
        GameObject manager =  GameObject.Find("BookManager");
        manager.tag = bookname;
        DontDestroyOnLoad(manager);
        SceneManager.LoadScene(2);
    }
    

    public void OnSelect()
    {
        Debug.Log("Loade scene script function called on air tap");
    }
    
}
