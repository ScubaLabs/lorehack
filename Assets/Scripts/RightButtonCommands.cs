﻿using UnityEngine;

public class RightButtonCommands : MonoBehaviour
{
    public GameObject bookController;

    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        bookController.GetComponent<BookController>().nextPage();
    }
}